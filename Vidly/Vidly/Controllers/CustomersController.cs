﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private MyDbContext _context;

        public CustomersController()
        {
            _context = new MyDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Customer
        public ViewResult CustomerList()
        {
            var customers = _context.Customers.Include(c => c.MembershipType).ToList();

            var viewModel = new MovieCustomerViewModel()
            {
                Customers = customers
            };

            return View(viewModel);
        }

        [Route("Customers/CustomerData/{id:regex(\\d{1}):range(1, 2)}")]
        public ActionResult CustomerData(int id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).Where(a => a.Id == id).FirstOrDefault();

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }

        public ActionResult NewCustomer()
        {
            var membershipTypes = _context.MembershipType.ToList();

            var viewModel = new CustomerMembersshipTypeViewModel()
            {
                Customer = new Customer(),
                MembershipTypes = membershipTypes
            };

            return View("CustomerForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                var viewModel = new CustomerMembersshipTypeViewModel()
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipType.ToList()
                };
                return View("CustomerForm", viewModel);
            }

            if (customer.Id == 0)
                _context.Customers.Add(customer);
            else
            {
                var customerInDb = _context.Customers.Single(c => c.Id == customer.Id);

                customerInDb.Name = customer.Name;
                customerInDb.BirthDate = customer.BirthDate;
                customerInDb.MembershipTypeId = customer.MembershipTypeId;
                customerInDb.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
            }

            _context.SaveChanges();
            return RedirectToAction("CustomerList", "Customers");
        }

        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.SingleOrDefault(a => a.Id == id);
            var membershipTypes = _context.MembershipType.ToList();

            var viewModel = new CustomerMembersshipTypeViewModel()
            {
                Customer = customer,
                MembershipTypes = membershipTypes
            };

            return View("CustomerForm", viewModel);
        }
    }
}