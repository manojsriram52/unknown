﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class MoviesController : Controller
    {
        private MyDbContext _context;

        public MoviesController()
        {
            _context = new MyDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }
        // GET: Movies
        public ViewResult MovieList()
        {
            var movies = _context.Movies.Include(m => m.Genre).ToList();

            var viewModel = new MovieCustomerViewModel()
            {
                Movies = movies
            };

            return View(viewModel);
        }

        [Route("Movies/MovieData/{id}")]
        public ActionResult MovieData(int id)
        {
            var movie = _context.Movies.Include(m => m.Genre).Where(a => a.Id == id).FirstOrDefault();

            if (movie == null)
                return HttpNotFound();

            return View(movie);
        }

        public ActionResult NewMovie()
        {
            var genres = _context.Genre.ToList();

            var viewModel = new MovieGenreViewModel()
            {
                Genres = genres
            };

            return View("MovieForm", viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Movie movie)
        {
            if (!ModelState.IsValid)
            { 
                var viewModel = new MovieGenreViewModel()
                {
                    Genres = _context.Genre.ToList()
                };
                return View("MovieForm", viewModel);
            }

            if (movie.Id == 0)
            {
                _context.Movies.Add(movie);
            }
            else
            {
                var movieInDb = _context.Movies.Single(c => c.Id == movie.Id);

                movieInDb.Name = movie.Name;
                movieInDb.ReleaseDate = movie.ReleaseDate;
                movieInDb.GenreId = movie.GenreId;
                movieInDb.NumberInStock = movie.NumberInStock;
            }
            _context.SaveChanges();
            return RedirectToAction("MovieList", "Movies");
        }

        public ActionResult Edit(int id)
        {
            var movie = _context.Movies.SingleOrDefault(a => a.Id == id);
            var genres = _context.Genre.ToList();

            var viewModel = new MovieGenreViewModel(movie)
            {
                Genres = genres
            };

            return View("MovieForm", viewModel);
        }
    }
}