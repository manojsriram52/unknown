﻿namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingNewGenre : DbMigration
    {
        public override void Up()
        {
            Sql("Insert Into Genres(Name) Values('Thriller')");
        }
        
        public override void Down()
        {
        }
    }
}
